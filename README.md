Mediawiki
=========

This Ansible role exposes a MediaWiki application through a PHP-FPM pool ans a lighttpd
simple webserver. It doesn't handle the database setup.

Requirements
------------

This role s designed for a Debian server or container. (tested on Buster)

Role Variables
--------------

| Name | Value | Default | Description |
|------|-------|---------|-------------|
| php_version | string | `php7.3` | php and php-fpm versions installed |
| mediawiki_version | string | `1.35` | Mediawiki version deployed |
| mediawiki_release_number | string | `0` | Release number of Mediawiki version |
| mediawiki_files_dir | string | `/var/www/mediawiki` | Mediawiki directory files deployed |
| mediawiki_web_user | string | `www-data` | User owning MediaWiki web files |

Dependencies
------------

No dependencies.

Example Playbook
----------------

```yaml
- hosts: servers
  roles:
  - role: mediawiki
    php_version: 7.3
    mediawiki_version: 1.35
    mediawiki_release_number: 0
    mediawiki_files_dir: /var/www/mediawiki
    mediawiki_web_user: www-data
```

License
-------

Author Information
------------------
