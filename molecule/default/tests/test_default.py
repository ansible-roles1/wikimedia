"""Role testing files using testinfra."""
PHP_VERSION=7.3

def test_php_dependencies_presence(host):
  """Test php packages and modules presence"""
  deps = [
    "php",
    "php-fpm",
    "php-apcu",
    "php-intl",
    "php-mbstring",
    "php-xml",
    "php-mysql",
  ]

  for dep in deps:
    assert host.package(dep).is_installed

def test_php_fpm_service_running(host):
  """Test php-fpm service running"""
  service = host.service("php%s-fpm" % (PHP_VERSION,))
  assert service.is_running

def test_webserver_200_response(host):
  """Test listening on all IPv4 and IPv6 hosts on port 80"""
  assert host.socket("tcp://0.0.0.0:80").is_listening
  assert host.socket("tcp://:::80").is_listening
